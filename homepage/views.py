from django.shortcuts import render
from scoreboard.models import Score
from news.models import NewsArticle


def index(request):
    response = {'scoreboard': Score.objects.all().values(),
                'articles': NewsArticle.objects.all().values()
    }
    return render(request, "homepage.html", response)
    